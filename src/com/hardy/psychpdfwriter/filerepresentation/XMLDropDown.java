package com.hardy.psychpdfwriter.filerepresentation;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 
 * Description: Represents a drop down list for display.
 * 
 * Author: Hardy Cherry
 **/
public class XMLDropDown extends XMLObject {

	public static final String DROPDOWN_NODE_NAME = "DropDown";
	public static final String TITLE_ATTR = "title";
	public static final String PROMPT_ATTR = "prompt";
	public static final String SELECTED_ATTR = "selected";
	
	private String title;
	private String prompt;
	private String selectedId;
	private List<XMLOption> options;
	
	public XMLDropDown() {

		super();
		title = "";
		prompt = "";
		selectedId = "";
		options = new ArrayList<>();
	}

	public XMLOption getItemById(String itemId) {
		
		XMLOption result = null;
		
		for(int i = 0; (i < options.size()) && (result == null); i++) {
			
			if(itemId.equals(options.get(i).getId())) {
				
				result = options.get(i);
			}
		}
		
		return result;
	}
	
	public boolean replaceItem(String id, XMLOption xmlObj) {
		
		boolean replacedItem = false;
		
		for(int i = 0; (i < options.size()) && !replacedItem; i++) {
			
			if(id.equals(options.get(i).getId())) {
				
				options.remove(i);
				options.add(i, xmlObj);
				replacedItem = true;
			}
		}	
		
		return replacedItem;
	}
	
	public String getPrompt() {
	
		return prompt;
	}

	
	public void setPrompt(String prompt) {
	
		this.prompt = prompt;
	}

	
	public String getSelectedId() {
	
		return selectedId;
	}

	
	public void setSelectedId(String selectedId) {
	
		this.selectedId = selectedId;
	}

	
	public List<XMLOption> getOptions() {
	
		return options;
	}

	
	public void setOptions(List<XMLOption> options) {
	
		this.options = options;
	}

	public String getTitle() {

		return title;
	}


	public void setTitle(String title) {

		this.title = title;
	}

	@Override
	public void parseXML(Node xml, List<String> errors) {
		
		if (DROPDOWN_NODE_NAME.equals(xml.getNodeName())) {

			NamedNodeMap attrs = xml.getAttributes();
			if (attrs != null) {

				setId(attrs.getNamedItem(ID_ATTR).getNodeValue());				
				setTitle(attrs.getNamedItem(TITLE_ATTR).getNodeValue());
				setPrompt(attrs.getNamedItem(PROMPT_ATTR).getNodeValue());
				setSelectedId(attrs.getNamedItem(SELECTED_ATTR).getNodeValue());
			}
			
			for(int i = 0; i < xml.getChildNodes().getLength(); i++) {
				
				Node n = xml.getChildNodes().item(i);

				if(XMLOption.OPTION_NODE_NAME.equals(n.getNodeName())) {
					
					XMLOption xmlOpt = new XMLOption();
					xmlOpt.parseXML(n, errors);
					options.add(xmlOpt);
				}
			}
		}
	}

	@Override
	public String getNodeName() {

		return DROPDOWN_NODE_NAME;
	}

	@Override
	public Element asPDFElement(PdfWriter writer, Font f, boolean includeEmptyFields) {

		String selectedId = getSelectedId();
		
		Paragraph p = new Paragraph();
		p.setFont(f);
		
		if(selectedId != null && selectedId.length() > 0) {
			
			XMLOption o = getItemById(selectedId);
			Element e = o.asPDFElement(writer, f, includeEmptyFields);
			if(e != null) {
				
				p.add(title + ": ");
				p.add(e);
			} else { 
				
				p = null;
			}
		}
		
		return p;
	}
}
