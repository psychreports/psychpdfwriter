package com.hardy.psychpdfwriter.filerepresentation;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 
 * Description: Represents a table for display.
 * 
 * Author: Hardy Cherry
 **/
public class XMLTable extends XMLObject {

	public static final String TABLE_NODE_NAME = "Table";
	public static final String TITLE_ATTR = "title";
	public static final String DESCRIPTION_ATTR = "description";
	public static final String INPUT_TYPE_ATTR = "inputType";
	public static final String COLUMN_COUNT_ATTR = "columnCount";
	public static final int DEFAULT_TABLE_COLUMN_COUNT = 2;
	public static final int MAX_TABLE_COLUMN_COUNT = 20;
	
	private String title;
	private String description;
	private int tableColumnCount;
	private List<XMLTableRow> tableRows;

	public XMLTable() {

		super();
		title = "";
		description = "";
		tableRows = new ArrayList<>();
		tableColumnCount = DEFAULT_TABLE_COLUMN_COUNT;
	}

	public XMLObject getItemById(String itemId) {
		
		XMLObject result = null;
		
		for(int i = 0; (i < tableRows.size()) && (result == null); i++) {
			
			if(itemId.equals(tableRows.get(i).getId())) {
				
				result = tableRows.get(i);
			} else {
				
				result = tableRows.get(i).getItemById(itemId);
			}
		}
		
		return result;
	}
	
	public boolean replaceItem(String id, XMLTableRow xmlObj) {
		
		boolean replacedItem = false;
		
		for(int i = 0; (i < tableRows.size()) && !replacedItem; i++) {
			
			if(id.equals(tableRows.get(i).getId())) {
				
				tableRows.remove(i);
				tableRows.add(i, xmlObj);
				replacedItem = true;
			}
		}	
		
		return replacedItem;
	}

	public String getTitle() {

		return title;
	}

	public void setTitle(String title) {

		this.title = title;
	}

	public String getDescription() {

		return description;
	}

	public void setDescription(String description) {

		this.description = description;
	}

	public List<XMLTableRow> getTableRows() {

		return tableRows;
	}

	public void setTableRows(List<XMLTableRow> tableRows) {

		this.tableRows = tableRows;
	}
		
	public int getColumnCount() {
		
		if(tableColumnCount < 1) {
			
			tableColumnCount = DEFAULT_TABLE_COLUMN_COUNT;
		} else if(tableColumnCount > MAX_TABLE_COLUMN_COUNT) {
			
			tableColumnCount = MAX_TABLE_COLUMN_COUNT;
		}		
		
		return tableColumnCount;
	}
	
	public void setColumnCount(int columnCount) {
		
		this.tableColumnCount = columnCount;
	}
	
	@Override
	public void parseXML(Node xml, List<String> errors) {

		if (TABLE_NODE_NAME.equals(xml.getNodeName())) {
		
			NamedNodeMap attrs = xml.getAttributes();
			if(attrs != null) {
				
				try {
					setId(attrs.getNamedItem(ID_ATTR).getNodeValue());
					setTitle(attrs.getNamedItem(TITLE_ATTR).getNodeValue());
					setDescription(attrs.getNamedItem(DESCRIPTION_ATTR).getNodeValue());
					int colCount = DEFAULT_TABLE_COLUMN_COUNT;
					try {
						String colCountStr = attrs.getNamedItem(COLUMN_COUNT_ATTR).getNodeValue();
						colCount = Integer.parseInt(colCountStr);
					} catch(NumberFormatException | NullPointerException e) {
						;//do nothing, just use default.
					}
					setColumnCount(colCount);
				} catch(Exception e) { 
					
					errors.add(xml.getNodeName() + " : " + e.getMessage());
				}
			}	
			
			for(int i = 0; i < xml.getChildNodes().getLength(); i++) {
				
				Node n = xml.getChildNodes().item(i);

				if(XMLTableRow.TABLE_ROW_NODE_NAME.equals(n.getNodeName())) {
					
					XMLTableRow row = new XMLTableRow();
					row.parseXML(n, errors);
					tableRows.add(row);
				}
			}
		}
	}

	@Override
	public String getNodeName() {

		return TABLE_NODE_NAME;
	}

	@Override
	public Element asPDFElement(PdfWriter writer, Font f, boolean includeEmptyFields) {

		if(includeEmptyFields || tableRows.size() > 0) {
			
			PdfPTable table = new PdfPTable(tableColumnCount);
			table.setWidthPercentage(100.0f);
			
			table.addCell(getTitle());
			table.addCell(getDescription());
			table.completeRow();
			
			for(int i = 0; i < tableRows.size(); i++) {
				
				Element e = tableRows.get(i).asPDFElement(writer, f, includeEmptyFields);
				if(e != null) {
				
					PdfPCell cell = new PdfPCell();
					cell.setBorder(0);
					cell.setPadding(0);
					cell.addElement(e);
					table.addCell(cell);
				}
			}
					
			return table;
		}
		
		return null;
	}
}
