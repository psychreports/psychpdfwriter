package com.hardy.psychpdfwriter.filerepresentation;

import java.util.List;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import com.hardy.psychpdfwriter.filerepresentation.constants.Font;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 
 * Description: Represents a label for display
 * 
 * Author: Hardy Cherry
 **/
public class XMLLabel extends XMLObject {

	public static final String LABEL_NODE_NAME = "Label";
	public static final String CONTENTS_ATTR = "contents";
	public static final String FONT_SIZE_ATTR = "fontSize";
	
	private String contents;
	private int fontSize;

	public XMLLabel() {

		super();
		contents = "";
		fontSize = Font.DEFAULT_SIZE;
	}

	public String getContents() {

		return contents;
	}

	public void setContents(String contents) {

		this.contents = contents;
	}

	public int getFontSize() {

		return fontSize;
	}

	public void setFontSize(int fontSize) {

		this.fontSize = fontSize;
	}

	@Override
	public void parseXML(Node xml, List<String> errors) {

		if (LABEL_NODE_NAME.equals(xml.getNodeName())) {
		
			NamedNodeMap attrs = xml.getAttributes();
			if(attrs != null) {
				
				try {
					setId(attrs.getNamedItem(ID_ATTR).getNodeValue());
					setContents(attrs.getNamedItem(CONTENTS_ATTR).getNodeValue());
					setFontSize(Integer.parseInt(attrs.getNamedItem(FONT_SIZE_ATTR).getNodeValue()));
				} catch(Exception e) { 
					
					errors.add(xml.getNodeName() + " : " + e.getMessage());
				}
			}				
		}
	}

	@Override
	public String getNodeName() {

		return LABEL_NODE_NAME;
	}

	@Override
	public Element asPDFElement(PdfWriter writer, com.itextpdf.text.Font f, boolean includeEmptyFields) {

		if(includeEmptyFields || !"".equals(getContents())) {
			
			float oldSize = f.getSize();
			
			Paragraph paragraph = new Paragraph(getContents());
			f.setSize(getFontSize());
			paragraph.setFont(f);
			
			f.setSize(oldSize);
			
			return paragraph;
		}
		
		return null;
	}
}
