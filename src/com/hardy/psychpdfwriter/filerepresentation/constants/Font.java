package com.hardy.psychpdfwriter.filerepresentation.constants;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 
 * Description: Contains constant values for Fonts.
 * 
 * Author: Hardy Cherry
 **/
public class Font {

	public static final int DEFAULT_SIZE = 12;
	public static final int LARGE_SIZE = 14;
	public static final int TITLE_SIZE = 16;
	public static final int SMALL_SIZE = 10;
}
