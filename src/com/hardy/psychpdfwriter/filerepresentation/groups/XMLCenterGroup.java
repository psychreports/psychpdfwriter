package com.hardy.psychpdfwriter.filerepresentation.groups;

import java.util.List;

import org.w3c.dom.Node;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 
 * Description: Represents a group of items to be centered.
 * 
 * Author: Hardy Cherry
 **/
public class XMLCenterGroup extends XMLGroup {

	public static final String CENTERGROUP_NODE_NAME = "CenterGroup";
	
	public XMLCenterGroup() {

		super();
	}

	@Override
	public void parseXML(Node xml, List<String> errors) {

		parseXML(xml, errors, CENTERGROUP_NODE_NAME);
	}
		
	@Override
	public String getNodeName() {

		return CENTERGROUP_NODE_NAME;
	}	
}
