package com.hardy.psychpdfwriter.filerepresentation.groups;

import java.util.List;

import org.w3c.dom.Node;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 
 * Description: A vertical group representation. The items are displayed
 * 
 * Author: Hardy Cherry
 **/
public class XMLVerticalGroup extends XMLGroup {

	public static final String VERTICALGROUP_NODE_NAME = "VerticalGroup";
	
	public XMLVerticalGroup() {

		super();
	}
	
	@Override
	public void parseXML(Node xml, List<String> errors) {

		parseXML(xml, errors, VERTICALGROUP_NODE_NAME);
	}
	
	@Override
	public String getNodeName() {

		return VERTICALGROUP_NODE_NAME;
	}
}
