package com.hardy.psychpdfwriter.filerepresentation.groups;

import java.util.List;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import com.hardy.psychpdfwriter.filerepresentation.XMLObject;
import com.hardy.psychpdfwriter.filerepresentation.error.XMLObjectCreationException;
import com.hardy.psychpdfwriter.filerepresentation.factory.XMLObjectFactory;
import com.hardy.psychpdfwriter.filerepresentation.groups.XMLGroup;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 
 * Description:
 * 
 * Author: Hardy Cherry
 **/
public class XMLTab extends XMLGroup {

	public static final String TAB_NODE_NAME = "Tab";
	public static final String INCLUDE_IN_REPORT_ATTR = "includeInReport";
	public static final String TITLE_ATTR = "title";

	protected String title;
	
	public XMLTab() {
		
		super();
		title = "";
	}
	
	
	public String getTitle() {
	
		return title;
	}

	
	public void setTitle(String title) {
	
		this.title = title;
	}

	@Override
	public void parseXML(Node xml, List<String> errors) {

		if (TAB_NODE_NAME.equals(xml.getNodeName())) {

			NamedNodeMap attrs = xml.getAttributes();
			if (attrs != null) {

				setId(attrs.getNamedItem(ID_ATTR).getNodeValue());	
				setIncludeInReport(attrs.getNamedItem(INCLUDE_IN_REPORT_ATTR).getNodeValue());
				setTitle(attrs.getNamedItem(TITLE_ATTR).getNodeValue());
			}
			
			for(int i = 0; i < xml.getChildNodes().getLength(); i++) {
				
				Node n = xml.getChildNodes().item(i);

				try {
					XMLObject xmlObj = XMLObjectFactory.getInstance(n.getNodeName());
					if(xmlObj != null) {
						
						xmlObj.parseXML(n, errors);
						itemNames.add(xmlObj.getId());
						items.put(xmlObj.getId(), xmlObj);
					}
				} catch(XMLObjectCreationException e) {
					
					errors.add(n.getNodeName() + " : " + e.getMessage());
				}
			}
		}
	}

	@Override
	public String getNodeName() {

		return TAB_NODE_NAME;
	}
}
