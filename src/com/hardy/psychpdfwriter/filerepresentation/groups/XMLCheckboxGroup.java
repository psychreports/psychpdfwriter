package com.hardy.psychpdfwriter.filerepresentation.groups;

import java.util.List;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import com.hardy.psychpdfwriter.filerepresentation.XMLObject;
import com.hardy.psychpdfwriter.filerepresentation.error.XMLObjectCreationException;
import com.hardy.psychpdfwriter.filerepresentation.factory.XMLObjectFactory;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 
 * Description: Represents a group of checkboxes for display.
 * 
 * Author: Hardy Cherry
 **/
public class XMLCheckboxGroup extends XMLGroup {

	public static final String CHECKBOXGROUP_NODE_NAME = "CheckBoxGroup";
	public static final String DESCRIPTION_ATTR = "description";
	public static final String FILL_HORIZONTAL_ATTR = "fillHorizontal";
	
	private String description;
	private boolean fillHorizontal;

	public XMLCheckboxGroup() {

		super();
		description = "";
		fillHorizontal = false;
	}

	public String getDescription() {

		return description;
	}

	public void setDescription(String description) {

		this.description = description;
	}

	public boolean isFillHorizontal() {

		return fillHorizontal;
	}

	public void setFillHorizontal(boolean fillHorizontal) {

		this.fillHorizontal = fillHorizontal;
	}

	@Override
	public void parseXML(Node xml, List<String> errors) {

		if (CHECKBOXGROUP_NODE_NAME.equals(xml.getNodeName())) {

			NamedNodeMap attrs = xml.getAttributes();
			if (attrs != null) {

				try {
					
					setId(attrs.getNamedItem(ID_ATTR).getNodeValue());	
					setDescription(attrs.getNamedItem(DESCRIPTION_ATTR).getNodeValue());
					setFillHorizontal(Boolean.parseBoolean(attrs.getNamedItem(FILL_HORIZONTAL_ATTR).getNodeValue()));
				} catch(Exception e) {
					
					errors.add(xml.getNodeName() + " : " + e.getMessage());
				}
			}
			
			for(int i = 0; i < xml.getChildNodes().getLength(); i++) {
				
				Node n = xml.getChildNodes().item(i);

				try {
					XMLObject xmlObj = XMLObjectFactory.getInstance(n.getNodeName());
					if(xmlObj != null) {
						
						xmlObj.parseXML(n, errors);
						itemNames.add(xmlObj.getId());
						items.put(xmlObj.getId(), xmlObj);
					}
				} catch(XMLObjectCreationException e) {
					
					errors.add(n.getNodeName() + " : " + e.getMessage());
				}
			}
		}
	}

	@Override
	public String getNodeName() {

		return CHECKBOXGROUP_NODE_NAME;
	}
}
