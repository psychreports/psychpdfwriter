package com.hardy.psychpdfwriter.filerepresentation.groups;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import com.hardy.psychpdfwriter.filerepresentation.XMLDropDown;
import com.hardy.psychpdfwriter.filerepresentation.XMLObject;
import com.hardy.psychpdfwriter.filerepresentation.XMLOption;
import com.hardy.psychpdfwriter.filerepresentation.XMLTableData;
import com.hardy.psychpdfwriter.filerepresentation.groups.XMLTab;
import com.hardy.psychpdfwriter.filerepresentation.XMLTable;
import com.hardy.psychpdfwriter.filerepresentation.XMLTableRow;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 
 * Description: Represents a group of items to be displayed
 * 
 * Author: Hardy Cherry
 **/
public class XMLTabPane extends XMLObject {

	public static final String TABPANE_NODE_NAME = "TabPane";
	public static final String INCLUDE_IN_REPORT_ATTR = "includeInReport";
	
	protected List<String> itemNames;
	protected Map<String, XMLTab> tabs;
	protected String includeInReport;

	public XMLTabPane() {

		super();
		itemNames = new ArrayList<>();
		tabs = new HashMap<>();
		includeInReport = "";
	}

	public XMLObject getItemById(String itemId) {
		
		XMLObject result = null;
		
		if(tabs.containsKey(itemId)) {
			
			result = tabs.get(itemId);
		} else {
			
			//search all children groups for the id
			Iterator<? extends XMLObject> itemIter = tabs.values().iterator();
			while(itemIter.hasNext() && result == null) {
				
				XMLObject xmlObj = itemIter.next();
				if(xmlObj instanceof XMLGroup) {
					
					result = ((XMLGroup)xmlObj).getItemById(itemId);
				} else if(xmlObj instanceof XMLDropDown) {
					
					result = ((XMLDropDown)xmlObj).getItemById(itemId);
				} else if(xmlObj instanceof XMLTable) {
					
					result = ((XMLTable)xmlObj).getItemById(itemId);
				} else if(xmlObj instanceof XMLTableRow) {
					
					result = ((XMLTableRow)xmlObj).getItemById(itemId);
				} else if(xmlObj instanceof XMLTabPane) {
					
					result = ((XMLTabPane)xmlObj).getItemById(itemId);
				} 	
			}
		}
		
		return result;
	}
	
	public boolean replaceItem(String itemId, XMLObject xmlObj) {
		
		boolean replacedObject = false;
		
		if(tabs.containsKey(itemId)) {
			
			tabs.remove(itemId);
			tabs.put(itemId, (XMLTab)xmlObj);
			replacedObject = true;
		} else {
		
			//search all children groups for the id
			Iterator<? extends XMLObject> itemIter = tabs.values().iterator();
			while(itemIter.hasNext() && !replacedObject) {
				
				XMLObject tempObj = itemIter.next();
				if(tempObj instanceof XMLGroup) {
					
					replacedObject = ((XMLGroup)tempObj).replaceItem(itemId, xmlObj);
				} else if(tempObj instanceof XMLDropDown) {
					
					if(xmlObj instanceof XMLOption) {
						
						replacedObject = ((XMLDropDown)tempObj).replaceItem(itemId, (XMLOption)xmlObj);
					}
				} else if(tempObj instanceof XMLTable) {
					
					if(xmlObj instanceof XMLTableRow) {
						
						replacedObject = ((XMLTable)tempObj).replaceItem(itemId, (XMLTableRow)xmlObj);
					}
				} else if(tempObj instanceof XMLTableRow) {
					
					if(xmlObj instanceof XMLTableData) {
						
						replacedObject = ((XMLTableRow)tempObj).replaceItem(itemId, (XMLTableData)xmlObj);
					}
				} else if(tempObj instanceof XMLTabPane) {
					
					replacedObject = ((XMLTabPane)tempObj).replaceItem(itemId, xmlObj);
				} 	
			}
		}
		
		return replacedObject;
	}
	
	public List<String> getItemNames() {

		return itemNames;
	}

	public void setItemNames(List<String> itemNames) {

		this.itemNames = itemNames;
	}

	public Map<String, XMLTab> getTabs() {

		return tabs;
	}

	public void setItems(Map<String, XMLTab> tabs) {

		this.tabs = tabs;
	}
	
	public String getIncludeInReport() {
		
		return includeInReport;
	}
	
	public void setIncludeInReport(String includeInReport) {
		
		this.includeInReport = includeInReport;
	}
	
	@Override
	public void parseXML(Node xml, List<String> errors) {
		
		if (TABPANE_NODE_NAME.equals(xml.getNodeName())) {

			NamedNodeMap attrs = xml.getAttributes();
			if (attrs != null) {

				setId(attrs.getNamedItem(ID_ATTR).getNodeValue());	
				setIncludeInReport(attrs.getNamedItem(INCLUDE_IN_REPORT_ATTR).getNodeValue());
			}
			
			for(int i = 0; i < xml.getChildNodes().getLength(); i++) {
				
				Node n = xml.getChildNodes().item(i);
				
				XMLTab xmlTab = new XMLTab();
				xmlTab.parseXML(n, errors);
				if(!"".equals(xmlTab.getId())) {
					itemNames.add(xmlTab.getId());
					tabs.put(xmlTab.getId(), xmlTab);
				}
			}
		}
	}

	@Override
	public String getNodeName() {

		return TABPANE_NODE_NAME;
	}

	@Override
	public Element asPDFElement(PdfWriter writer, Font f, boolean includeEmptyFields) {

		if(includeEmptyFields || itemNames.size() > 0) {
		
			Paragraph p = new Paragraph();
			
			if(!"false".equalsIgnoreCase(includeInReport)) {
			
				for(int i = 0; i < itemNames.size(); i++) {
					
					p.add(tabs.get(itemNames.get(i)).asPDFElement(writer, f, includeEmptyFields));
				}
			}			
			return p;
		}
		
		return null;
	}
}
