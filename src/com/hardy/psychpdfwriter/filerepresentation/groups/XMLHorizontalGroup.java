package com.hardy.psychpdfwriter.filerepresentation.groups;

import java.util.List;

import org.w3c.dom.Node;

import com.hardy.psychpdfwriter.filerepresentation.XMLObject;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 
 * Description: Represents a horizontal group of items for display. The items
 *              are aligned in a row when possible.
 * 
 * Author: Hardy Cherry
 **/
public class XMLHorizontalGroup extends XMLGroup {

	public static final String HORIZONTALGROUP_NODE_NAME = "HorizontalGroup";
	
	public XMLHorizontalGroup() {

		super();
	}
	
	@Override
	public void parseXML(Node xml, List<String> errors) {

		parseXML(xml, errors, HORIZONTALGROUP_NODE_NAME);
	}

	@Override
	public String getNodeName() {

		return HORIZONTALGROUP_NODE_NAME;
	}
	
	@Override
	public Element asPDFElement(PdfWriter writer, Font f, boolean includeEmptyFields) {

		if(includeEmptyFields || itemNames.size() > 0) {
		
			PdfPTable table = new PdfPTable(itemNames.size());
			table.setWidthPercentage(100.0f);
		
			if(!"false".equalsIgnoreCase(includeInReport)) {
				
				for(int i = 0; i < itemNames.size(); i++) {
					
					XMLObject o = items.get(itemNames.get(i));
					Element e = o.asPDFElement(writer, f, includeEmptyFields);
					if(e != null) {
					
						PdfPCell c = new PdfPCell();
						c.setBorder(0);
						c.addElement(e);
						table.addCell(c);
					}
				}
			}
			return table;
		}
		
		return null;
	}
}
