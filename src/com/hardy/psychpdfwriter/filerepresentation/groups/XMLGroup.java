package com.hardy.psychpdfwriter.filerepresentation.groups;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import com.hardy.psychpdfwriter.filerepresentation.XMLDropDown;
import com.hardy.psychpdfwriter.filerepresentation.XMLObject;
import com.hardy.psychpdfwriter.filerepresentation.XMLOption;
import com.hardy.psychpdfwriter.filerepresentation.XMLTable;
import com.hardy.psychpdfwriter.filerepresentation.XMLTableData;
import com.hardy.psychpdfwriter.filerepresentation.XMLTableRow;
import com.hardy.psychpdfwriter.filerepresentation.error.XMLObjectCreationException;
import com.hardy.psychpdfwriter.filerepresentation.factory.XMLObjectFactory;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 
 * Description: A group of items for display. No special formatting is applied
 *              to the items in the group. Just an ordering based on position in
 *              the group.
 * 
 * Author: Hardy Cherry
 **/
public class XMLGroup extends XMLObject {

	public static final String GROUP_NODE_NAME = "Group";
	public static final String INCLUDE_IN_REPORT_ATTR = "includeInReport";

	protected List<String> itemNames;
	protected Map<String, XMLObject> items;
	protected String includeInReport;
	
	public XMLGroup() {

		super();
		itemNames = new ArrayList<>();
		items = new HashMap<>();
		includeInReport = "true";
	}

	public XMLObject getItemById(String itemId) {

		XMLObject result = null;

		if (items.containsKey(itemId)) {

			result = items.get(itemId);
		} else {

			// search all children groups for the id
			Iterator<? extends XMLObject> itemIter = items.values().iterator();
			while (itemIter.hasNext() && result == null) {

				XMLObject xmlObj = itemIter.next();
				if(xmlObj instanceof XMLGroup) {
					
					result = ((XMLGroup)xmlObj).getItemById(itemId);
				} else if(xmlObj instanceof XMLDropDown) {
					
					result = ((XMLDropDown)xmlObj).getItemById(itemId);
				} else if(xmlObj instanceof XMLTable) {
					
					result = ((XMLTable)xmlObj).getItemById(itemId);
				} else if(xmlObj instanceof XMLTableRow) {
					
					result = ((XMLTableRow)xmlObj).getItemById(itemId);
				} else if(xmlObj instanceof XMLTabPane) {
					
					result = ((XMLTabPane)xmlObj).getItemById(itemId);
				} 	
			}
		}

		return result;
	}

	public boolean replaceItem(String itemId, XMLObject xmlObj) {
		
		boolean replacedObject = false;
		
		if(items.containsKey(itemId)) {
			
			items.remove(itemId);
			items.put(itemId, xmlObj);
			replacedObject = true;
		} else {
		
			//search all children groups for the id
			Iterator<? extends XMLObject> itemIter = items.values().iterator();
			while(itemIter.hasNext() && !replacedObject) {
				
				XMLObject tempObj = itemIter.next();
				if(tempObj instanceof XMLGroup) {
					
					replacedObject = ((XMLGroup)tempObj).replaceItem(itemId, xmlObj);
				} else if(tempObj instanceof XMLDropDown) {
					
					if(xmlObj instanceof XMLOption) {
						
						replacedObject = ((XMLDropDown)tempObj).replaceItem(itemId, (XMLOption)xmlObj);
					}
				} else if(tempObj instanceof XMLTable) {
					
					if(xmlObj instanceof XMLTableRow) {
						
						replacedObject = ((XMLTable)tempObj).replaceItem(itemId, (XMLTableRow)xmlObj);
					}
				} else if(tempObj instanceof XMLTableRow) {
					
					if(xmlObj instanceof XMLTableData) {
						
						replacedObject = ((XMLTableRow)tempObj).replaceItem(itemId, (XMLTableData)xmlObj);
					}
				} else if(tempObj instanceof XMLTabPane) {
					
					replacedObject = ((XMLTabPane)tempObj).replaceItem(itemId, xmlObj);
				} 	
			}
		}
		
		return replacedObject;
	}

	public List<String> getItemNames() {

		return itemNames;
	}

	public void setItemNames(List<String> itemNames) {

		this.itemNames = itemNames;
	}

	public Map<String, XMLObject> getItems() {

		return items;
	}

	public void setItems(Map<String, XMLObject> items) {

		this.items = items;
	}

	public String getIncludeInReport() {

		return includeInReport;
	}

	public void setIncludeInReport(String includeInReport) {

		this.includeInReport = includeInReport;
	}

	@Override
	public void parseXML(Node xml, List<String> errors) {

		parseXML(xml, errors, GROUP_NODE_NAME);
	}

	protected void parseXML(Node xml, List<String> errors, String nodeName) {

		if (nodeName.equals(xml.getNodeName())) {

			NamedNodeMap attrs = xml.getAttributes();
			if (attrs != null) {

				setId(attrs.getNamedItem(ID_ATTR).getNodeValue());
				setIncludeInReport(attrs.getNamedItem(INCLUDE_IN_REPORT_ATTR).getNodeValue());
			}

			for (int i = 0; i < xml.getChildNodes().getLength(); i++) {

				Node n = xml.getChildNodes().item(i);

				try {
					XMLObject xmlObj = XMLObjectFactory.getInstance(n.getNodeName());
					if (xmlObj != null) {

						xmlObj.parseXML(n, errors);
						itemNames.add(xmlObj.getId());
						items.put(xmlObj.getId(), xmlObj);
					}
				} catch (XMLObjectCreationException e) {

					errors.add(n.getNodeName() + " : " + e.getMessage());
				}
			}
		}
	}

	@Override
	public String getNodeName() {

		return GROUP_NODE_NAME;
	}

	@Override
	public Element asPDFElement(PdfWriter writer, Font f, boolean includeEmptyFields) {

		if(includeEmptyFields || itemNames.size() > 0) {
			PdfPTable table = new PdfPTable(1);
			table.setWidthPercentage(100.0f);
			
			if(!"false".equalsIgnoreCase(includeInReport)) {
						
				for(int i = 0; i < itemNames.size(); i++) {
					
					XMLObject o = items.get(itemNames.get(i));
					Element e = o.asPDFElement(writer, f, includeEmptyFields);
					if(e != null) {
						
						PdfPCell c = new PdfPCell();
						if(this instanceof XMLCenterGroup) {
							
							c.setHorizontalAlignment(Element.ALIGN_CENTER);
						}
						c.setBorder(0);
						c.addElement(e);
						table.addCell(c);
						table.completeRow();
					}
				}
			}
			return table;
		}
		
		return null;
	}
}
