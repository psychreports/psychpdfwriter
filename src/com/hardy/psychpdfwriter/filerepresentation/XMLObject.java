package com.hardy.psychpdfwriter.filerepresentation;

import java.util.List;

import org.w3c.dom.Node;

import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 
 * Description: the base xml object that all other xml objects should extend.
 * 
 * Author: Hardy Cherry
 **/
public abstract class XMLObject {

	public static final String ID_ATTR = "id";
	
	private String id;

	public XMLObject() {

		id = "";
	}

	public String getId() {

		return id;
	}

	public void setId(String id) {

		this.id = id;
	}
	
	/**
	 * Takes xml and parses it into the current class.
	 * @param xml the xml node to parse.
	 * @param errors the errors object, all errors that occure will be stored in this
	 * if it's empty after the parse there were no problems.
	 */
	public abstract void parseXML(Node xml, List<String> errors);

	/**
	 * @return the name of the tag that would be displayed in the xml.
	 */
	public abstract String getNodeName();
	
	/**
	 * @param writer the writer
	 * @param the current font
	 * @param includeEmptyFields true if we should include blank or empty fields
	 * @return the xml object as a pdf element for display
	 */
	public abstract Element asPDFElement(PdfWriter writer, Font f, boolean includeEmptyFields);
}
