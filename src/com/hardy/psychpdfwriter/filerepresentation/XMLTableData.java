package com.hardy.psychpdfwriter.filerepresentation;

import java.util.List;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import com.hardy.psychpdfwriter.filerepresentation.error.XMLObjectCreationException;
import com.hardy.psychpdfwriter.filerepresentation.factory.XMLObjectFactory;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 
 * Description: Represents table data for display
 * 
 * Author: Hardy Cherry
 **/
public class XMLTableData extends XMLObject {

	public static final String TABLE_DATA_NODE_NAME = "td";
	
	private XMLObject data;

	public XMLTableData() {

		super();
		data = null;
	}

	public XMLObject getData() {

		return data;
	}

	public void setData(XMLObject data) {

		this.data = data;
	}
	
	@Override
	public void parseXML(Node xml, List<String> errors) {

		if (TABLE_DATA_NODE_NAME.equals(xml.getNodeName())) {
		
			XMLObject n = null;
			try {
								
				Node node = xml.getFirstChild();
				String nodeName = "";
				while(n == null) {
					
					nodeName = node.getNodeName();
					n = XMLObjectFactory.getInstance(nodeName);
					
					if(n == null) {
						node = node.getNextSibling();
					}
				}
				
				n.parseXML(node, errors);
				setData(n); 
			} catch(XMLObjectCreationException e) {
				
				errors.add(e.getMessage());
			}
			
			NamedNodeMap attrs = xml.getAttributes();
			if(attrs != null) {
				
				try {
					
					setId(attrs.getNamedItem(ID_ATTR).getNodeValue());					
				} catch(Exception e) { 
					
					errors.add(xml.getNodeName() + " : " + e.getMessage());
				}
			}				
		}
	}
	
	@Override
	public String getNodeName() {

		return TABLE_DATA_NODE_NAME;
	}

	@Override
	public Element asPDFElement(PdfWriter writer, Font f, boolean includeEmptyFields) {

		return data.asPDFElement(writer, f, includeEmptyFields);
	}
}
