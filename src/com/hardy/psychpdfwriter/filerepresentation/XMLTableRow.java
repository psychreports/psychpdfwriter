package com.hardy.psychpdfwriter.filerepresentation;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 
 * Description: Represents a table row
 * 
 * Author: Hardy Cherry
 **/
public class XMLTableRow extends XMLObject {

	public static final String TABLE_ROW_NODE_NAME = "tr";
	
	private List<XMLTableData> tableData;

	public XMLTableRow() {

		super();
		tableData = new ArrayList<>();
	}

	public XMLObject getItemById(String itemId) {
		
		XMLObject result = null;
		
		for(int i = 0; (i < tableData.size()) && (result == null); i++) {
			
			if(itemId.equals(tableData.get(i).getId())) {
				
				result = tableData.get(i);
			} else {
				
				if(tableData.get(i).getData() != null && 
						itemId.equals(tableData.get(i).getData().getId())) {
					
					result = tableData.get(i).getData();
				}
			}
		}
		
		return result;
	}
	
	public boolean replaceItem(String id, XMLTableData xmlObj) {

		boolean replacedItem = false;
		
		for(int i = 0; (i < tableData.size()) && !replacedItem; i++) {
			
			if(id.equals(tableData.get(i).getId())) {
				
				tableData.remove(i);
				tableData.add(i, xmlObj);
				replacedItem = true;
			}
		}
		
		return replacedItem;
	}
	
	public void add(XMLTableData data) {
		
		tableData.add(data);
	}
	
	public List<XMLTableData> getTableData() {

		return tableData;
	}

	public void setTableData(List<XMLTableData> tableData) {

		this.tableData = tableData;
	}
	
	@Override
	public void parseXML(Node xml, List<String> errors) {

		if (TABLE_ROW_NODE_NAME.equals(xml.getNodeName())) {
		
			NamedNodeMap attrs = xml.getAttributes();
			if(attrs != null) {
				
				try {
					setId(attrs.getNamedItem(ID_ATTR).getNodeValue());					
				} catch(Exception e) { 
					
					errors.add(xml.getNodeName() + " : " + e.getMessage());
				}
			}		
			
			for(int i = 0; i < xml.getChildNodes().getLength(); i++) {
				
				Node n = xml.getChildNodes().item(i);

				if(XMLTableData.TABLE_DATA_NODE_NAME.equals(n.getNodeName())) {
					
					XMLTableData data = new XMLTableData();
					data.parseXML(n, errors);
					tableData.add(data);
				}
			}
		}
	}

	@Override
	public String getNodeName() {

		return TABLE_ROW_NODE_NAME;
	}

	@Override
	public Element asPDFElement(PdfWriter writer, Font f, boolean includeEmptyFields) {

		if(includeEmptyFields || tableData.size() > 0) {
		
			PdfPTable table = new PdfPTable(tableData.size());
			table.setWidthPercentage(100.0f);
			
			for(int i = 0; i < tableData.size(); i++) {
				
				Element e = tableData.get(i).asPDFElement(writer, f, includeEmptyFields);
				if(e != null) {
				
					PdfPCell cell = new PdfPCell();
					cell.addElement(e);
					table.addCell(cell);
				}
			}
			
			return table;
		} 
		
		return null;
	}
}
