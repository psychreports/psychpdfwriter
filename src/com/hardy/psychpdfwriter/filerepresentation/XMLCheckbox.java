package com.hardy.psychpdfwriter.filerepresentation;

import java.util.List;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseField;
import com.itextpdf.text.pdf.GrayColor;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.RadioCheckField;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 
 * Description: Represents a checkbox for display.
 * 
 * Author: Hardy Cherry
 **/
public class XMLCheckbox extends XMLObject {

	public static final String CHECKBOX_NODE_NAME = "CheckBox";
	public static final String DESCRIPTION_ATTR = "description";
	public static final String CHECKED_ATTR = "checked";
	
	private String description;
	private boolean checked;

	public XMLCheckbox() {

		super();
		description = "";
		checked = false;
	}

	public String getDescription() {

		return description;
	}

	public void setDescription(String description) {

		this.description = description;
	}

	public boolean isChecked() {

		return checked;
	}

	public void setChecked(boolean checked) {

		this.checked = checked;
	}

	@Override
	public void parseXML(Node xml, List<String> errors) {

		if (CHECKBOX_NODE_NAME.equals(xml.getNodeName())) {
		
			NamedNodeMap attrs = xml.getAttributes();
			if(attrs != null) {
				
				try {
					setId(attrs.getNamedItem(ID_ATTR).getNodeValue());
					setDescription(attrs.getNamedItem(DESCRIPTION_ATTR).getNodeValue());
					setChecked(Boolean.parseBoolean(attrs.getNamedItem(CHECKED_ATTR).getNodeValue()));
				} catch(Exception e) { 
					
					errors.add(xml.getNodeName() + " : " + e.getMessage());
				}
			}				
		}
	}

	@Override
	public String getNodeName() {

		return CHECKBOX_NODE_NAME;
	}

	@Override
	public Element asPDFElement(PdfWriter writer, Font f, boolean includeEmptyFields) {

		if(includeEmptyFields || isChecked()) {
			
			PdfPTable table = new PdfPTable(2);
			table.setWidthPercentage(100.0f);
			
			PdfPCell cell = new PdfPCell();
			cell.setMinimumHeight(15f);
			cell.setBorder(0);
			
			String onValue = "on";
			if(isChecked()) {
				onValue = "off";
			}
			
			cell.setCellEvent(new CBCellEvent(writer, f, getDescription(), onValue));
			table.addCell(cell);
			
			PdfPCell description = new PdfPCell();
			description.addElement(new Paragraph(getDescription()));
			description.setBorder(0);
			table.addCell(description);
			
			return table;
		}
		
		return null;
	}
	
	private class CBCellEvent implements PdfPCellEvent {

		private PdfWriter writer;
		private Font f;
		private String description;
		private String onValue;
		private CBCellEvent(PdfWriter writer, Font f, String description, String onValue) {
			
			this.writer = writer;
			this.f = f;
			this.description = description;
			this.onValue = onValue;
		} 
		
		@Override
		public void cellLayout(PdfPCell cell, Rectangle position, PdfContentByte[] canvases) {
			
			float llx = position.getLeft();// + ((position.getRight() -  position.getLeft()) / 2f) - 5f;
			float lly = position.getBottom() + ((position.getTop() -  position.getBottom()) / 2f) - 8f;
			try
			{
				RadioCheckField rcf = new RadioCheckField(writer, new Rectangle(llx, lly, llx + 15, lly + 15), description, onValue);
				
				rcf.setCheckType(RadioCheckField.TYPE_CHECK);
				rcf.setFont(f.getBaseFont());
				rcf.setBorderColor(GrayColor.GRAYBLACK);
				rcf.setBackgroundColor(GrayColor.GRAYWHITE);
				rcf.setCheckType(RadioCheckField.TYPE_CHECK);
				rcf.setBorderWidth(BaseField.BORDER_WIDTH_THIN);
				rcf.setChecked(isChecked());
            	 
				PdfFormField ff = rcf.getCheckField();
				ff.setFieldFlags(PdfFormField.FF_READ_ONLY);
				writer.addAnnotation(ff);
			} catch (Exception e) {
				;
			} 		
		}		
	}
}
