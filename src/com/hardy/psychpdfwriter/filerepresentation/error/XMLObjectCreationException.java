package com.hardy.psychpdfwriter.filerepresentation.error;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 
 * Description: exception class
 * 
 * Author: Hardy Cherry
 **/
public class XMLObjectCreationException extends Exception {

	private static final long serialVersionUID = 1L;

	public XMLObjectCreationException(String message) {
		
		super(message);
	}
}
