package com.hardy.psychpdfwriter.filerepresentation.error;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 
 * Description:
 * 
 * Author: Hardy Cherry
 **/
public class XMLObjectUpdateException extends Exception {

	/**
	 * default serial id
	 */
	private static final long serialVersionUID = 1L;

	public XMLObjectUpdateException(String message) {
		
		super(message);
	}
}
