package com.hardy.psychpdfwriter.filerepresentation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 
 * Description: Represents a Date field for display.
 * 
 * Author: Hardy Cherry
 **/
public class XMLDateField extends XMLObject {

	public static final String DATEFIELD_NODE_NAME = "DateField";
	public static final String DESCRIPTION_ATTR = "description";
	public static final String CONTENTS_ATTR = "contents";

	private String description;
	private Date contents;
	
	public XMLDateField() {

		super();
		description = "";
		contents = null;
	}

	public String getDescription() {

		return description;
	}

	public void setDescription(String description) {

		this.description = description;
	}

	public Date getContents() {

		return contents;
	}

	public void setContents(Date contents) {

		this.contents = contents;
	}

	public void setContents(String date) {

		SimpleDateFormat f = new SimpleDateFormat("EEE MMM d hh:mm:ss z yyyy");
		try {
			if(date != null && !"".equals(date) && !"null".equalsIgnoreCase(date)) {
				setContents(f.parse(date));
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void parseXML(Node xml, List<String> errors) {

		if (DATEFIELD_NODE_NAME.equals(xml.getNodeName())) {

			NamedNodeMap attrs = xml.getAttributes();
			if (attrs != null) {

				try {
					setId(attrs.getNamedItem(ID_ATTR).getNodeValue());
					setDescription(attrs.getNamedItem(DESCRIPTION_ATTR).getNodeValue());
					setContents(attrs.getNamedItem(CONTENTS_ATTR).getNodeValue());
				} catch (Exception e) {

					errors.add(xml.getNodeName() + " : " + e.getMessage());
				}
			}
		}
	}

	@Override
	public String getNodeName() {

		return DATEFIELD_NODE_NAME;
	}

	@Override
	public Element asPDFElement(PdfWriter writer, Font f, boolean includeEmptyFields) {

		if(includeEmptyFields || !"".equals(getContents())) {
		
			PdfPTable table = new PdfPTable(2);
			table.setWidthPercentage(100.0f);
			
			PdfPCell description = new PdfPCell(new Phrase(getDescription()));
			description.setBorder(0);
			
			Paragraph paragraph = new Paragraph();
			SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy hh a");
			if(getContents() != null) {
				
				paragraph.add(format.format(getContents()));
			}
			
			PdfPCell contents = new PdfPCell(paragraph);
			contents.setBorder(0);
			
			table.addCell(description);
			table.addCell(contents);
			
			return table;
		}
		
		return null;
	}
}
