package com.hardy.psychpdfwriter.filerepresentation;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.hardy.psychpdfwriter.filerepresentation.error.XMLObjectCreationException;
import com.hardy.psychpdfwriter.filerepresentation.factory.XMLObjectFactory;
import com.hardy.psychpdfwriter.filerepresentation.groups.XMLGroup;
import com.hardy.psychpdfwriter.filerepresentation.groups.XMLTabPane;
import com.hardy.psychpdfwriter.utils.PsychReportUtils;
import com.hardy.psychpdfwriter.dom.ParseableDom;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 
 * Description: represents a file loaded from the filesystem contians all the
 *              xml objects defined in the file.
 * 
 * Author: Hardy Cherry
 **/
public class XMLFile implements ParseableDom {

	public static final String REPORT_EXT = ".report";
	public static final String TEMPLATE_EXT = ".template";
	
	private static Logger log = Logger.getLogger(XMLFile.class);
	
	public static final String REPORT_NODE = "Report";
	public static final String SETTINGS_NODE = "Settings";
	public static final String SEARCH_INFO_NODE = "SearchInfo";
	public static final String REPORT_SAVED_DATE = "ReportSavedDate";
	public static final String FIELDS_NODE = "Fields";
	
	
	private List<String> itemIds;
	private Map<String, XMLObject> items;
	private Date reportSavedDate;
	
	public XMLFile() {

		reportSavedDate = null;
		itemIds = new ArrayList<>();
		items = new HashMap<String, XMLObject>();
	}

	public void addItem(String itemId, XMLObject item) {

		itemIds.add(itemId);
		items.put(itemId, item);
	}

	/**
	 * @param itemId
	 *            the item to look up
	 * @return an xmlObject or null
	 */
	public XMLObject getItemById(String itemId) {

		XMLObject result = null;

		if (items.containsKey(itemId)) {

			result = items.get(itemId);
		} else {
			
			//search all children groups for the id
			Iterator<? extends XMLObject> itemIter = items.values().iterator();
			while(itemIter.hasNext() && result == null) {
				
				XMLObject xmlObj = itemIter.next();
				if(xmlObj instanceof XMLGroup) {
					
					result = ((XMLGroup)xmlObj).getItemById(itemId);
				} else if(xmlObj instanceof XMLDropDown) {
					
					result = ((XMLDropDown)xmlObj).getItemById(itemId);
				} else if(xmlObj instanceof XMLTable) {
					
					result = ((XMLTable)xmlObj).getItemById(itemId);
				} else if(xmlObj instanceof XMLTableRow) {
					
					result = ((XMLTableRow)xmlObj).getItemById(itemId);
				} else if(xmlObj instanceof XMLTabPane) {
					
					result = ((XMLTabPane)xmlObj).getItemById(itemId);
				} 			
			}
		}

		return result;
	}
	
	public Element getPDFElements(PdfWriter writer, Font f, boolean includeEmptyFields) {
		
		Paragraph result = new Paragraph();
		
		Iterator<String> itemIdIter = getItemIdIterator();
		while(itemIdIter.hasNext()) {
			
			String id = itemIdIter.next();
			XMLObject obj = items.get(id);
			result.add(obj.asPDFElement(writer, f, includeEmptyFields));
		}
				
		return result;
	}
	
	public Iterator<String> getItemIdIterator() {

		return itemIds.iterator();
	}

	public Date getReportSavedDate() {

		if(reportSavedDate == null) {
			reportSavedDate = new Date();
		}
		
		return reportSavedDate;
	}

	public void setReportSavedDate(Date reportSavedDate) {

		this.reportSavedDate = reportSavedDate;
	}
	
	public void parseXML(Node xml, List<String> errors) {
		
		for(int i = 0; i < xml.getChildNodes().getLength(); i++) {
			
			Node childNode = xml.getChildNodes().item(i);
			switch(childNode.getNodeName()) {
				
				case SETTINGS_NODE:
					break;
				case FIELDS_NODE:
					parseFields(childNode, errors);
					break;
				case SEARCH_INFO_NODE:
					break;
				case "#text":
					//this covers the case when there are \t, \n or \r to format the xml.
					break;
				default:
					errors.add("Unable to parse node: " + childNode.getNodeName());
			}
		}
	}
	
	private void parseFields(Node xml, List<String> errors) {
		
		for(int i = 0; i < xml.getChildNodes().getLength(); i++) {
		
			try {
				
				Node childNode = xml.getChildNodes().item(i);
				XMLObject obj = XMLObjectFactory.getInstance(childNode.getNodeName());
				if(obj != null) {
				
					obj.parseXML(childNode, errors);
					itemIds.add(obj.getId());
					items.put(obj.getId(), obj);
				}
			} catch(XMLObjectCreationException e) {
				
				errors.add(xml.getNodeName() + " : " + e.getMessage());
			}
		}			
	}
	
	@Override
	public void parse(String input) throws ParseException {

		List<String> errors = new ArrayList<>();
		
		try {
			
			InputStream stream = new ByteArrayInputStream(input.getBytes());
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(stream);
			doc.getDocumentElement().normalize();
			parseXML(doc.getFirstChild(), errors);
		} catch(Exception e) {
			
			log.error("Error converting string to Document: " + PsychReportUtils.getStackTrace(e));
			throw new ParseException("Unable to parse string", 0);
		}
		
		if(errors.size() > 0) {
			
			log.error("Errors parsing xml: " + PsychReportUtils.convertErrorsListToString(errors));
			throw new ParseException("Unable to parse xml. See Log for details", 0);
		}
	}
}
