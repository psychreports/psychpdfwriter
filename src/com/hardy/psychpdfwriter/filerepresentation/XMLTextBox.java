package com.hardy.psychpdfwriter.filerepresentation;

import java.util.List;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 
 * Description: Represents a textbox.
 * 
 * Author: Hardy Cherry
 **/
public class XMLTextBox extends XMLObject {

	public static final String TEXT_BOX_NODE_NAME = "TextBox";
	public static final String DESCRIPTION_ATTR = "description";
	public static final String CONTENTS_ATTR = "contents";
	
	private String description;
	private String contents;
	
	public XMLTextBox() {

		super();
		description = "";
		contents = "";
	}

	public String getDescription() {

		return description;
	}

	public void setDescription(String description) {

		this.description = description;
	}

	public String getContents() {

		return contents;
	}

	public void setContents(String contents) {

		this.contents = contents;
	}
	
	@Override
	public void parseXML(Node xml, List<String> errors) {

		if (TEXT_BOX_NODE_NAME.equals(xml.getNodeName())) {
		
			NamedNodeMap attrs = xml.getAttributes();
			if(attrs != null) {
				
				try {
					setId(attrs.getNamedItem(ID_ATTR).getNodeValue());
					setDescription(attrs.getNamedItem(DESCRIPTION_ATTR).getNodeValue());
					setContents(attrs.getNamedItem(CONTENTS_ATTR).getNodeValue());
				} catch(Exception e) { 
					
					errors.add(xml.getNodeName() + " : " + e.getMessage());
				}
			}				
		}
	}

	@Override
	public String getNodeName() {

		return TEXT_BOX_NODE_NAME;
	}

	@Override
	public Element asPDFElement(PdfWriter writer, Font f, boolean includeEmptyFields) {

		if(includeEmptyFields || !"".equals(getContents())) {
			PdfPTable table = new PdfPTable(2);
			table.setWidthPercentage(100.0f);
			
			Phrase p = new Phrase(getDescription());
			p.setFont(f);
			
			PdfPCell description = new PdfPCell(p);
			description.setBorder(0);		
			
			Paragraph paragraph = new Paragraph();
			paragraph.add(getContents());
			paragraph.setFont(f);
			
			PdfPCell contents = new PdfPCell(paragraph);
			contents.setBorder(0);
			
			table.addCell(description);
			table.addCell(contents);
			
			return table;
		}
		
		return null;
	}
}
