package com.hardy.psychpdfwriter.filerepresentation;

import java.io.IOException;
import java.io.StringReader;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import com.hardy.psychpdfwriter.utils.PsychReportUtils;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.html.simpleparser.HTMLWorker;
import com.itextpdf.text.html.simpleparser.StyleSheet;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 
 * Description: Represents a text area for display.
 * 
 * Author: Hardy Cherry
 **/
public class XMLTextArea extends XMLObject {

	private static Logger log = Logger.getLogger(XMLTextArea.class);
	
	public static final String TEXT_AREA_NODE_NAME = "TextArea";
	public static final String DESCRIPTION_ATTR = "description";
	public static final String CONTENTS_ATTR = "contents";
	
	private String description;
	private String contents;
	
	public XMLTextArea() {

		super();
		description = "";
		contents = "";
	}

	public String getDescription() {

		return description;
	}

	public void setDescription(String description) {

		this.description = description;
	}

	public String getContents() {

		return contents;
	}

	public void setContents(String contents) {

		this.contents = contents;
	}
	
	@Override
	public void parseXML(Node xml, List<String> errors) {

		if (TEXT_AREA_NODE_NAME.equals(xml.getNodeName())) {
		
			NamedNodeMap attrs = xml.getAttributes();
			if(attrs != null) {
				
				try {
					setId(attrs.getNamedItem(ID_ATTR).getNodeValue());
					setDescription(attrs.getNamedItem(DESCRIPTION_ATTR).getNodeValue());
					setContents(attrs.getNamedItem(CONTENTS_ATTR).getNodeValue());
				} catch(Exception e) { 
					
					errors.add(xml.getNodeName() + " : " + e.getMessage());
				}
			}				
		}
	}

	@Override
	public String getNodeName() {

		return TEXT_AREA_NODE_NAME;
	}

	@Override
	public Element asPDFElement(PdfWriter writer, Font f, boolean includeEmptyFields) {

		if(includeEmptyFields || !"".equals(getContents())) {
			
			PdfPTable table = new PdfPTable(1);
			table.setWidthPercentage(100.0f);
				
			List<Element> elements = null;
			try {
				String html = getContents();
				while(html.contains("<hr />") || html.contains("<hr>") || html.contains("<hr/>")) {
					html = html.replaceAll("<hr>", "<br />");
					html = html.replaceAll("<hr/>", "<br />");
					html = html.replaceAll("<hr />", "<br />");
				}
				StyleSheet ss = new StyleSheet();
				elements = HTMLWorker.parseToList(new StringReader(html), ss);
			} catch(IOException e) {
				
				log.error("Can't parse HTML: " + PsychReportUtils.getStackTrace(e));
			}
			
			Paragraph paragraph = new Paragraph();
			paragraph.setKeepTogether(false);
			paragraph.setFont(f);
	
			if(elements != null) {
				
				Iterator<Element> elementIter = elements.iterator();
				while(elementIter.hasNext()) {
					
					Element e = elementIter.next();
					paragraph.add(e);
				}			
			}
			
			Phrase p = new Phrase(getDescription());
			p.setFont(f);
			
			PdfPCell description = new PdfPCell(p);
			description.setBorder(0);
					
			PdfPCell contents = new PdfPCell(paragraph);
					
			table.addCell(description);
			table.completeRow();
			table.addCell(contents);
			
			return table;
		}
		
		return null;
	}
}
