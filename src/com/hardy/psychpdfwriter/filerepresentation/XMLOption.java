package com.hardy.psychpdfwriter.filerepresentation;

import java.util.List;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 
 * Description: Represents drop down option for display
 * 
 * Author: Hardy Cherry
 **/
public class XMLOption extends XMLObject {

	public static final String OPTION_NODE_NAME = "Option";
	public static final String CONTENTS_ATTR = "contents";
	
	private String contents;

	public XMLOption() {

		super();
		contents = "";
	}
	
	public void setContents(String contents) {
		
		this.contents = contents;
	}
	
	public String getContents() {
		
		return contents;
	}
	
	@Override
	public void parseXML(Node xml, List<String> errors) {

		if (OPTION_NODE_NAME.equals(xml.getNodeName())) {
			
			NamedNodeMap attrs = xml.getAttributes();
			if(attrs != null) {
				
				try {
					
					setId(attrs.getNamedItem(ID_ATTR).getNodeValue());
					setContents(attrs.getNamedItem(CONTENTS_ATTR).getNodeValue());					
				} catch(Exception e) { 
					
					errors.add(xml.getNodeName() + " : " + e.getMessage());
				}
			}				
		}
	}
	
	@Override
	public String toString() {
	
		return contents;
	}

	@Override
	public String getNodeName() {

		return OPTION_NODE_NAME;
	}

	@Override
	public Element asPDFElement(PdfWriter writer, Font f, boolean includeEmptyFields) {

		if(includeEmptyFields || !"".equals(getContents())) {
			
			Phrase p = new Phrase(getContents(), f);
			return p;
		}
		
		return null;
	}
}
