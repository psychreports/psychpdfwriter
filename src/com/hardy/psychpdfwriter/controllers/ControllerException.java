package com.hardy.psychpdfwriter.controllers;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 
 * Description:
 * 
 * Author: Hardy Cherry
 **/
public class ControllerException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ControllerException(String message) {
		
		super(message);
	}
}
