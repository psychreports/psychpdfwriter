package com.hardy.psychpdfwriter.controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.hardy.psychpdfwriter.PsychPDFWriter;
import com.hardy.psychpdfwriter.daos.FileAccess;
import com.hardy.psychpdfwriter.daos.ReportModel;
import com.hardy.psychpdfwriter.daos.error.FileAccessException;
import com.hardy.psychpdfwriter.daos.error.ModelException;
import com.hardy.psychpdfwriter.filerepresentation.XMLFile;
import com.hardy.psychpdfwriter.utils.PsychReportUtils;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.HyphenationAuto;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 
 * Description:
 * 
 * Author: Hardy Cherry
 **/
public class PsychPDFWriterController implements Initializable {

	private static Logger log = Logger.getLogger(PsychPDFWriterController.class);
	
	@FXML
	protected TextArea topLeftTextArea;
	
	@FXML
	protected Label leftErrorLbl;
	
	@FXML
	protected TextArea topRightTextArea;
	
	@FXML
	protected Label rightErrorLbl;
	
	@FXML
	protected TextArea headerTextArea;
	
	@FXML 
	protected CheckBox displayPageNumberChkbx;
	
	@FXML
	protected ImageView imageDisplay;
	
	@FXML
	protected TextArea footerTxt;
	
	@FXML
	protected Label errorLbl;
	
	@FXML
	protected CheckBox imageDisplayOnRigthChkBx;
	
	@FXML
	protected CheckBox emptyFieldsChkBx;
	
	private String imagePath = null;
		
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		topRightTextArea.textProperty().addListener(topRightTextAreaLengthChecker);
		topLeftTextArea.textProperty().addListener(topLeftTextAreaLengthChecker);
		footerTxt.textProperty().addListener(footerTextAreaLengthChecker);
	}
	
	protected ChangeListener<String> topRightTextAreaLengthChecker = new ChangeListener<String>() {
		
		@Override
		public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
		
			if(newValue != null && newValue.length() > 52) {
				
				topRightTextArea.setText(oldValue);
				errorLbl.setText("Text in the topleft or top right can't exceed 52 characters in length.");
			} else {
				
				errorLbl.setText("");
			}
		}
	}; 
	
	protected ChangeListener<String> topLeftTextAreaLengthChecker = new ChangeListener<String>() {
		
		@Override
		public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
		
			if(newValue != null && newValue.length() > 52) {
				
				topLeftTextArea.setText(oldValue);
				errorLbl.setText("Text in the topleft or top right can't exceed 52 characters in length.");
			} else {
				
				errorLbl.setText("");
			}
		}
	};
	
	protected ChangeListener<String> footerTextAreaLengthChecker = new ChangeListener<String>() {
		
		@Override
		public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
		
			if(newValue != null && newValue.length() > 513) {
				
				footerTxt.setText(oldValue);
				errorLbl.setText("Text in the footer can't exceed 513 characters in length.");
			} else {
				
				errorLbl.setText("");
			}
		}
	};
	
	@FXML
	protected void chooseReportAndExportPDF() throws ControllerException {
		
		FileChooser fc = new FileChooser();
		String reportsPath = FileAccess.getReportsDirectory();
		String pdfPath = FileAccess.getPDFDirectory();
		
		File reportsFile = new File(reportsPath);
		fc.setInitialDirectory(reportsFile);
		fc.setTitle("Pick a report to convert to a PDF.");
		fc.getExtensionFilters().add(new ExtensionFilter("PsychReport report file.", "*.report"));
		File reportFile = fc.showOpenDialog(PsychPDFWriter.getPrimaryStage());
		
		if(reportFile != null) {
			
			File pdfsFile = new File(pdfPath);
			FileChooser fc2 = new FileChooser();
			fc2.setInitialDirectory(pdfsFile);
			fc2.setTitle("Save PDF as...");
			fc2.getExtensionFilters().add(new ExtensionFilter("PDF file.", "*.pdf"));
			File pdfFile = fc2.showSaveDialog(PsychPDFWriter.getPrimaryStage());
			
			if(pdfFile != null) {
				
				if(pdfFile.getAbsolutePath().indexOf(".pdf") < 0) {
					
					pdfFile = new File(pdfFile.getAbsolutePath()+".pdf");
				}
				
				if(pdfFile.exists() && !pdfFile.isDirectory()) {
					
					pdfFile.delete();
				}
					
				try {
					
					pdfFile.createNewFile();
				} catch (IOException e) {

					log.error("Couldn't create new file: " + PsychReportUtils.getStackTrace(e));
				}
										
				if(pdfFile.exists() && reportFile.exists()) {
					
					FileAccess.setPDFDirectory(pdfFile.getAbsolutePath());
					FileAccess.setReportsDirectory(reportFile.getAbsolutePath());
					
					openReportAndConvert(reportFile, pdfFile);
				} else {
					
					throw new ControllerException("The report doesn't exist or the pdf file can't be created. Try a differen't report or try saving the pdf to a different location.");
				}
			}
		}		
	}
	
	private void openReportAndConvert(File reportFile, File pdfFile) {
		
		try {
		
			ReportModel.getInstance().initializeModel(reportFile);
			
			XMLFile xmlFile = ReportModel.getInstance().getXMLFile();
			
			Font f = new Font();
			f.setSize(10);
			f.setFamily("Times New Roman");
			f.setStyle("normal");
			f.setColor(BaseColor.BLACK);
						
			Document document = new Document(PageSize.A4, 60, 60, 120, 80);
			
			boolean includeEmptyFields = emptyFieldsChkBx.isSelected();
			
			FileOutputStream pdfFileStream = new FileOutputStream(pdfFile);
	        PdfWriter writer = PdfWriter.getInstance(document, pdfFileStream);

	        writer.setPageEvent(new HeaderAndFooter());
	        
	        document.open();
	        
	        document.add(getTitleInfo());
	        
		    document.add(xmlFile.getPDFElements(writer, f, includeEmptyFields));
	        
		    try {
		    	
		       writer.flush();
		       writer.close();
		    } catch(Exception e) {
		    	
		    	log.error("Error closing writer: " + PsychReportUtils.getStackTrace(e));
		    }
		    
	        document.close();
	        
	        pdfFileStream.flush();
	        pdfFileStream.close();
		} catch(DocumentException d) {
			
			log.error("Problem with itext: " + PsychReportUtils.getStackTrace(d));
		} catch(FileNotFoundException f) {
			
			log.error("Unable to init output pdf file: " + PsychReportUtils.getStackTrace(f));
		} catch(ModelException e) {
			
			log.error("unable to init model: " + PsychReportUtils.getStackTrace(e));
		} catch(IOException e) {
			
			log.error("IOException: " + PsychReportUtils.getStackTrace(e));
		} catch(Exception e1) {
			
			log.error("There was an error creating the PDF: " + PsychReportUtils.getStackTrace(e1));
		}
	}
	
	private Element getTitleInfo() {
		
		PdfPTable table = new PdfPTable(2);
		table.setWidthPercentage(100.0f);
		
		PdfPCell text = null;
		PdfPCell image = null;
		PdfPCell empty = new PdfPCell();
		empty.setBorder(0);
		
		if(headerTextArea.getText() != null && headerTextArea.getText().length() > 0) {
			
			text = new PdfPCell();
			text.setBorder(0);
			Paragraph p = new Paragraph(headerTextArea.getText());
			text.addElement(p);
		}
		
		if(imagePath != null) {
			
			try {
				
				com.itextpdf.text.Image pdfImage = com.itextpdf.text.Image.getInstance(imagePath);
				
				image = new PdfPCell();
				image.addElement(new Chunk(pdfImage, 0, -(pdfImage.getHeight())+25));
				image.setBorder(0);
				image.setMinimumHeight(pdfImage.getHeight());
			} catch(Exception e) {
				
				table.addCell(new Paragraph("Logo unavailable"));
			}
		}
		
		PdfPCell right = empty;
		PdfPCell left = empty;
		
		if(imageDisplayOnRigthChkBx.isSelected()) {
			
			if(text != null) {
				
				left = text;
			}
			if(image != null) {
				
				right = image;
			}
		} else {
			
			if(image != null) {
				
				left = image;
			}
			
			if(text != null) {
				
				right = text;
			} 
		}
		
		if(right == empty) {
			
			left.setColspan(2);
			left.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
			table.addCell(left);			
		} else if(left == empty) {
			
			right.setColspan(2);
			right.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
			table.addCell(right);
		} else {
			
			table.addCell(left);
			table.addCell(right);
		}
		
		return table;
	}
	
	@FXML
	protected void chooseImage() {
		
		try {
			FileChooser fc = new FileChooser();
			String rootPath = FileAccess.getRootPath();
			if(rootPath != null && rootPath.length() > 0) {
			
				File rootFile = new File(rootPath);
				fc.setInitialDirectory(rootFile);
				fc.setTitle("Pick a logo image.");
				fc.getExtensionFilters().add(new ExtensionFilter("Image files.", Arrays.asList("*.jpg", "*.png", "*.gif", "*.jpeg")));
				File logoImage = fc.showOpenDialog(PsychPDFWriter.getPrimaryStage());
				
				if(logoImage != null) {
					
					imagePath = logoImage.getAbsolutePath();
					Image logoImageImg = new Image(new FileInputStream(logoImage));
					imageDisplay.setImage(logoImageImg);
				}				
			}
		} catch(FileNotFoundException f) {
			
			imagePath = null;
			log.error("Can't find selected file: " + PsychReportUtils.getStackTrace(f));
		} catch(FileAccessException e) {
			
			imagePath = null;
			log.error("Error opening an image: " + PsychReportUtils.getStackTrace(e));
		}
	}
	
	private class HeaderAndFooter extends PdfPageEventHelper {
		@Override
		public void onStartPage(PdfWriter writer, Document document) {
		
			Rectangle rect = document.getPageSize();

			PdfPTable table = new PdfPTable(3);
			table.setWidthPercentage(100.0f);
			
			if(writer.getPageNumber() > 0) {
				if(displayPageNumberChkbx.isSelected()) {
					
					ColumnText.showTextAligned(writer.getDirectContent(),
							Element.ALIGN_RIGHT, new Phrase(String.format("%d", writer.getPageNumber())),
			                rect.getRight()-45, rect.getTop()-40, 0);
				}
				
				 
				ColumnText ct = new ColumnText(writer.getDirectContent());
				try {
					//one line supports 53 characters for now.
					Chunk c = new Chunk(topRightTextArea.getText());
					c.setHyphenation(new HyphenationAuto("en", "US", 2, 2));
					Paragraph trText = new Paragraph(c);
					ct.addText(trText);
					ct.setAlignment(Element.ALIGN_RIGHT);
					ct.setSimpleColumn(rect.getWidth()/2, rect.getTop()-80, rect.getRight()-45, rect.getTop()-50);
					ct.go();
				} catch(DocumentException e) {
					
					log.error("Error adding right header text: " + topRightTextArea.getText() + ": " + PsychReportUtils.getStackTrace(e));
				}
				
				try {
					
					Chunk c = new Chunk(topLeftTextArea.getText());
					c.setHyphenation(new HyphenationAuto("en", "US", 2, 2));
					Paragraph tlText = new Paragraph(c);
					ct.addText(tlText);
					ct.setAlignment(Element.ALIGN_LEFT);
					ct.setSimpleColumn(rect.getLeft()+60, rect.getTop()-80, rect.getWidth()/2, rect.getTop()-50);
					ct.go();
				} catch(DocumentException e) {
					
					log.error("Error adding left header text: " + topLeftTextArea.getText() + ": " + PsychReportUtils.getStackTrace(e));
				}
			}
		}
		
		@Override
		public void onEndPage(PdfWriter writer, Document document) {
		
			Rectangle rect = document.getPageSize();

			PdfPTable table = new PdfPTable(3);
			table.setWidthPercentage(100.0f);
			
			Font f = new Font();
			f.setSize(8f);
			
			ColumnText ct = new ColumnText(writer.getDirectContent());
			try {
				Chunk c = new Chunk(footerTxt.getText());
				c.setFont(f);
				c.setHyphenation(new HyphenationAuto("en", "US", 2, 2));
				Paragraph trText = new Paragraph(c);
				trText.setFont(f);
				ct.addText(trText);
				ct.setAlignment(Element.ALIGN_LEFT);
				ct.setSimpleColumn(45, rect.getBottom()+55, rect.getRight()-55, rect.getBottom()+120);
				ct.go();
			} catch(DocumentException e) {
				
				log.error("Error adding right header text: " + topRightTextArea.getText() + ": " + PsychReportUtils.getStackTrace(e));
			}
		}
	}
}
