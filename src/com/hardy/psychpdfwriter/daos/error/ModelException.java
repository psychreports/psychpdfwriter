package com.hardy.psychpdfwriter.daos.error;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 
 * Description:
 * 
 * Author: Hardy Cherry
 **/
public class ModelException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8375233423595451811L;

	public ModelException(String message) {
		
		super(message);
	}
}
