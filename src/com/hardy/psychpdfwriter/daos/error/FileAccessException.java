package com.hardy.psychpdfwriter.daos.error;

/**
 * Copyright 2012 
 * Hardy Cherry
 * Description:
 * 
 * Author: Hardy Cherry
 **/
public class FileAccessException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5471734449337755132L;

	public FileAccessException(String message) {
		
		super(message);
	}
}
