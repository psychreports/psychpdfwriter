package com.hardy.psychpdfwriter.daos;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.security.CodeSource;

import javax.xml.stream.Location;

import org.apache.commons.lang3.SystemUtils;
import org.apache.log4j.Logger;

import com.hardy.psychpdfwriter.daos.error.FileAccessException;
import com.hardy.psychpdfwriter.utils.PsychReportUtils;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 
 * Description: This class contains methods for opening and writing files. .
 * 
 * Author: Hardy Cherry
 **/
public class FileAccess {

	private static Logger log = Logger.getLogger(FileAccess.class);

	private static final String SETTINGS_FILE = "psychPDFWriterSettings.dat";
	public static final String REPORTS_DIR = "reportsDir=";
	public static final String PDF_DIR = "pdfDir=";
	
	/**
	 * The last directory a report was opened from.
	 */
	private static String reportsDirectory = "";
	
	/**
	 * The last directory a pdf was saved in.
	 */
	private static String pdfDirectory = "";
	
        /**
         * The directory to use if either the pdf or report dir is empty.
         */
        private static String defaultDirectory = "";
        
	/**
	 * Takes a file and reads the contents and returns them as a string.
	 * 
	 * @param f
	 *            the file to read
	 * @return the string contents of the file
	 */
	public static String readFile(File f) {

		FileInputStream fis = null;
		StringBuilder builder = new StringBuilder();
		try {

			fis = new FileInputStream(f);

			byte[] b = new byte[1024];
			int len = 0;
			while ((len = fis.read(b)) != -1) {

				for(int i = 0; i < len; i++) {
				
					builder.append((char)b[i]);
				}
			}
		} catch (FileNotFoundException e) {

			try {

				f.createNewFile();
			} catch (IOException e1) {

				log.error("Unable to create file: " + f.getPath());
			}
		} catch (Exception e) {

			log.error("An error occured while reading the file: " + PsychReportUtils.getStackTrace(e));
		} finally {

			try {

				if (fis != null) {

					fis.close();
				}
			} catch (IOException e) {

				log.error("Error closing the file: " + PsychReportUtils.getStackTrace(e));
			}
		}

		return builder.toString();
	}

	public static String getRootPath() throws FileAccessException {
		
		String location = "";
		CodeSource cs = Location.class.getProtectionDomain().getCodeSource();
		if (cs != null) {

			location = cs.getLocation().getPath();
		} else {

			log.error("Unable to get the location from the code source.");
			location = System.getProperty("user.dir");

			if (location == null || "".equals(location)) {
				
				log.error("Unable to get the location from System.getProperty");
				location = new java.io.File("").getAbsolutePath().toString();
				
				if(location == null || "".equals(location)) {
					
					log.error("Unable to get the location from file.getAbsolutePath");
				}
			}
		}
		
		if(location == null || "".equals(location)) {
			
			log.error("Unable to get the root location of the app");
			throw new FileAccessException("Unable to get the root location");
		}
		
		return location;
	}
	
	private static void setDefaults(String defaultDir) throws FileAccessException {
		
		reportsDirectory = defaultDir;
		pdfDirectory = defaultDir;
	}
	
	public static void loadSettings() {
		
		try {
			defaultDirectory = getRootPath() + File.separator;
			setDefaults(defaultDirectory);
                        
			File settings = new File(defaultDirectory + SETTINGS_FILE);
			if(settings.exists()) {
				
				String settingsStr = readFile(settings);
				if(settingsStr != null && settingsStr.length() > 0) {
					
					String[] settingsArray = settingsStr.split(SystemUtils.LINE_SEPARATOR);
					if(settingsArray != null && settingsArray.length > 0) {
					
						for(int i = 0; i < settingsArray.length; i++) {
							
							String line = settingsArray[i];
							if(line.startsWith(REPORTS_DIR)) {
								
								reportsDirectory = line.substring(REPORTS_DIR.length());
							} else if(line.startsWith(PDF_DIR)) {
								
								pdfDirectory = line.substring(PDF_DIR.length());
							}
						}
					}
				}
			}
			
			if(reportsDirectory == null || reportsDirectory.length() == 0) {
				
				setReportsDirectory(defaultDirectory);
			}
			
			if(pdfDirectory == null || pdfDirectory.length() == 0) {
				
				setPDFDirectory(defaultDirectory);
			}
		} catch(FileAccessException e) {
			
			log.error("Unable to load settings file: " + SETTINGS_FILE + "\n" + PsychReportUtils.getStackTrace(e));
		}
	}
	
	public static void saveSettings() {
		
		FileWriter fw = null;
		
		try {
			
			boolean fileExists = false;
			File settings = new File(getRootPath() + File.separator + SETTINGS_FILE);
			if(!settings.exists()) {
				
				fileExists = settings.createNewFile();
			} else {
				
				fileExists = true;
			}
			
			if(fileExists) {
				StringBuilder sb = new StringBuilder();
				sb.append(REPORTS_DIR).append(reportsDirectory).append(SystemUtils.LINE_SEPARATOR);
				sb.append(PDF_DIR).append(pdfDirectory).append(SystemUtils.LINE_SEPARATOR);
				
				fw = new FileWriter(settings);
				fw.write(sb.toString());
				fw.flush();
				fw.close();
			} else {
				
				log.error("Unable to create " + SETTINGS_FILE);
			}
		} catch(FileAccessException e) {
			
			log.error("Unable to load settings file: " + SETTINGS_FILE + "\n" + PsychReportUtils.getStackTrace(e));
		} catch (IOException e) {

			if(fw != null) {
				
				try {
					
					fw.close();
				} catch (IOException e1) {

					log.error("Unable to write or close settings file: " + PsychReportUtils.getStackTrace(e1));		
				}
			}
		}
	}
	
	public static String getReportsDirectory() {
                
                if("".equals(reportsDirectory)) {
                        
                        if("".equals(defaultDirectory)) {
                            
                            loadSettings();
                        } else {
                            
                            reportsDirectory = defaultDirectory;
                        }
                }
            
		return reportsDirectory;
	}
	
	public static String getPDFDirectory() {
		
            if("".equals(pdfDirectory)) {
                        
                        if("".equals(defaultDirectory)) {
                            
                            loadSettings();
                        } else {
                            
                            pdfDirectory = defaultDirectory;
                        }
                }
            
		return pdfDirectory;
	}
	
	public static void setReportsDirectory(String newDir) {
		
		if(newDir != null && newDir.length() > 0 &&
				!newDir.equals(reportsDirectory)) {

			if(!newDir.endsWith(File.separator)) {
				
				newDir = newDir.substring(0, newDir.lastIndexOf(File.separator)+1);
			}
			
			reportsDirectory = newDir;
			saveSettings();
		}
	}
	
	public static void setPDFDirectory(String newDir) {
		
		if(newDir != null && newDir.length() > 0 &&
				!newDir.equals(pdfDirectory)) {
			
			if(!newDir.endsWith(File.separator)) {
				
				newDir = newDir.substring(0, newDir.lastIndexOf(File.separator)+1);
			}
			
			pdfDirectory = newDir;
			saveSettings();
		}
	}
}
