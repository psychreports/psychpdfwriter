package com.hardy.psychpdfwriter.daos;

import java.io.File;
import java.text.ParseException;

import org.apache.log4j.Logger;

import com.hardy.psychpdfwriter.daos.error.ModelException;
import com.hardy.psychpdfwriter.filerepresentation.XMLFile;
import com.hardy.psychpdfwriter.utils.PsychReportUtils;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 
 * Description:
 * 
 * Author: Hardy Cherry
 **/
public class ReportModel {

	private static Logger log = Logger.getLogger(ReportModel.class);

	private static ReportModel reportModel;
	private XMLFile xmlFile;
	private boolean dirty;

	public static ReportModel getInstance() {

		if (reportModel == null) {

			reportModel = new ReportModel();
		}

		return reportModel;
	}

	private ReportModel() {

		xmlFile = new XMLFile();
	}
	
	public void close() {
		
		reportModel = null;
		xmlFile = null;
		dirty = false;
	}

	public void initializeModel(File file) throws ModelException {

		String fileContents = FileAccess.readFile(file);

		try {
			xmlFile.parse(fileContents);
		} catch (ParseException e) {
			
			log.error(PsychReportUtils.getStackTrace(e));
			throw new ModelException("Unable to parse provided file");
		}
	}
		
	public final XMLFile getXMLFile() {
		
		return xmlFile;
	}
	
	public boolean isDirty() {
		
		return dirty;
	}
}
