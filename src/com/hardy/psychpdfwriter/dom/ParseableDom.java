package com.hardy.psychpdfwriter.dom;

import java.text.ParseException;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 
 * Description: An interface that defines what a class needs to have to be able to parse a string.
 * 
 * Author: Hardy Cherry
 **/
public interface ParseableDom {

	/**
	 * Takes a string and parses it populating the class with the data the string contains.
	 * @param input the data for the class
	 * @throws ParseException if there are any problems parsing the string.
	 */
	public void parse(String input) throws ParseException;
}
