package com.hardy.psychpdfwriter.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBoxBuilder;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 
 * Description:
 * 
 * Author: Hardy Cherry
 **/
public class PsychReportUtils {

	/**
	 * Takes a throwable object and converts each line to a string.
	 * 
	 * @param t
	 *            the object to convert to a string
	 * @return a string representation of all the stacktrace elements in the
	 *         throwable object.
	 */
	public static String getStackTrace(Throwable t) {

		StringBuilder result = new StringBuilder();

		StackTraceElement[] elements = t.getStackTrace();
		result.append(System.lineSeparator());
		if(t.getCause() != null) {
			
			result.append(t.getCause().getMessage());
		}
		
		for (int i = 0; i < elements.length; i++) {

			StackTraceElement e = elements[i];
			result.append(e.toString()).append(System.lineSeparator());

			// tack the message on the first time through
			if (i == 0) {
				result.append(" ").append(t.getMessage()).append(":");
			}

			// add a tab for every line after the first.
			result.append("\t");
		}

		return result.toString();
	}

	/**
	 * Takes a list of strings and converts it to one long string with new lines
	 * between each error.
	 * 
	 * @param errors
	 *            the list to convert
	 * @return the string representation of the list.
	 */
	public static String convertErrorsListToString(List<String> errors) {

		StringBuilder results = new StringBuilder();

		for (int i = 0; i < errors.size(); i++) {

			results.append(errors.get(i)).append(System.lineSeparator());
		}

		return results.toString();
	}
	
	public static Date convertStringToDate(String dateStr) throws ParseException {
		
		Date result = null;
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
		try {
			
			result = format.parse(dateStr);
		} catch(ParseException e) {
			
			try {
				
				format = new SimpleDateFormat("MM-dd-yyyy");
				result = format.parse(dateStr);
			} catch(ParseException e1) {
				
				try {
					
					format = new SimpleDateFormat("dd/MM/yyyy");
					result = format.parse(dateStr);
				} catch(ParseException e2) {
				
					try {
						
						format = new SimpleDateFormat("dd-MM-yyyy");
						result = format.parse(dateStr);
					} catch(ParseException e3) {
					
						try {
							
							format = new SimpleDateFormat("dd-MMM-yyyy");
							result = format.parse(dateStr);
						} catch(ParseException e4) {
						
							try {
								
								format = new SimpleDateFormat("dd/MMM/yyyy");
								result = format.parse(dateStr);
							} catch(ParseException e5) {
							
								throw e5;
							}	
						}	
					}	
				}	
			}			
		}
		
		return result;
	}
	
	private static Stage dialogStage;
	public static void displayError(String error) {
		
		javafx.event.EventHandler<ActionEvent> okHandler = new javafx.event.EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {

				okClick(event);				
			}
		};
		
		Button okButton = new Button("Ok");
		okButton.setOnAction(okHandler);
		
		dialogStage = new Stage();
		dialogStage.initModality(Modality.WINDOW_MODAL);
		dialogStage.requestFocus();
		dialogStage.setScene(new Scene(VBoxBuilder.create().
		    children(new Text(error), okButton).
		    alignment(Pos.CENTER).padding(new Insets(5)).build()));
		dialogStage.centerOnScreen();
		dialogStage.setTitle("Error");
		dialogStage.show();
	}
	
	private static void okClick(ActionEvent event) {
		
		dialogStage.hide();
		dialogStage.close();
		dialogStage = null;
	}
}
