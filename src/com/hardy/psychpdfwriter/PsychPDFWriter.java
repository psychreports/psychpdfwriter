package com.hardy.psychpdfwriter;

import java.io.File;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import org.apache.log4j.PropertyConfigurator;

import com.hardy.psychpdfwriter.daos.FileAccess;
import com.hardy.psychpdfwriter.daos.error.FileAccessException;

/**
 * Copyright 2012 
 * Hardy Cherry
 * 
 * Description:
 * 
 * Author: Hardy Cherry
 **/
public class PsychPDFWriter extends Application {
		
	private static Stage primaryStage;
	
	@Override
	public void start(Stage primaryStage) throws Exception {

		primaryStage.getIcons().add(new Image(PsychPDFWriter.class.getResourceAsStream("icons/32x32icon.png")));
		
		PsychPDFWriter.primaryStage = primaryStage;
		
		Scene scene = null;
		
		Parent root = FXMLLoader.load(PsychPDFWriter.class.getResource("views/PsychPDFWriter.fxml"));
		scene = new Scene(root, 612.0, 580.0);
		
		primaryStage.setTitle("Psych PDF Writer");
		primaryStage.setMaxHeight(670.0);
		primaryStage.setMaxWidth(612.0);
		primaryStage.setMinHeight(670.0);
		primaryStage.setMinWidth(612.0);
		primaryStage.close();
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	public static Stage getPrimaryStage() {
		
		return primaryStage;
	}
	
	/**
	 * The main() method is ignored in correctly deployed JavaFX application.
	 * main() serves only as fallback in case the application can not be
	 * launched through deployment artifacts, e.g., in IDEs with limited FX
	 * support. NetBeans ignores main().
	 * 
	 * @param args
	 *            the command line arguments
	 * @throws FileAccessException 
	 */
	public static void main(String[] args) throws FileAccessException {

		//Set up logging folder
		String logDirStr = "log";
		File logDir = new File(logDirStr);		
		logDir.mkdirs();
		
		String iniFilePath = "psychpdfwriter.ini";
		
		// Set up a simple configuration that logs on the console.
	    PropertyConfigurator.configure(iniFilePath);
		
	    FileAccess.loadSettings();
	    
		launch(args);
	}
}
